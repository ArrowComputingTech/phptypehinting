<?php

  class Posts {
    private $postContent = "";
    public $posts;

    function __construct() {
      $this->posts = [];
    }

    function savePost(string $content) : void {
      array_push($this->posts, $content);
      echo "Post saved: {$content}<br>";
    }

    function fetchPosts() : array {
      return $this->posts;
    }
  }

  $p = new Posts();
  $p->savePost("This is a demo blog. It has bloggy things and blog blog blog.");
  $p->savePost("Another day, another blog post.");
  var_dump($p->fetchPosts());

?>
